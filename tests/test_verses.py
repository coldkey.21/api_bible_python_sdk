import pytest
from api_bible_sdk import APIBible
from unittest.mock import Mock, patch

chapter_verses_response = '{"data": [{"id": "MAT.1.1", "reference": "Matthew 1:1"}, {"id": "MAT.1.2", "reference": "Matthew 1:2"}]}'
chapter_verses_return = [
    {"id": "MAT.1.1", "reference": "Matthew 1:1"},
    {"id": "MAT.1.2", "reference": "Matthew 1:2"},
]

verses_response = '{ "data": { "id": "MAT.1.1", "bibleId": "de4e12af7f28f599-01", "reference": "Matthew 1:1" } }'
verses_return = {
    "id": "MAT.1.1",
    "bibleId": "de4e12af7f28f599-01",
    "reference": "Matthew 1:1",
}


@patch("api_bible_sdk.api_bible.requests.get")
def test_chapter_verses(mock_get):
    mock_get().text = chapter_verses_response
    api_bible = APIBible("1234")
    chapter_verses = api_bible.get_chapter_verses("de4e12af7f28f599-01", "MAT.1")
    assert len(chapter_verses) == 2
    assert chapter_verses == chapter_verses_return


@patch("api_bible_sdk.api_bible.requests.get")
def test_verses(mock_get):
    mock_get().text = verses_response
    api_bible = APIBible("1234")
    verses = api_bible.get_verses("de4e12af7f28f599-01", "MAT.1.1")
    assert verses == verses_return
