import pytest
from api_bible_sdk import APIBible
from unittest.mock import Mock, patch

chapters_response = (
    '{"data":[{"id":"GEN.intro","number":"intro"},{"id":"GEN.1","number":"1"}]}'
)
chapters_return = [
    {"id": "GEN.intro", "number": "intro"},
    {"id": "GEN.1", "number": "1"},
]

chapter_response = '{"data":{"id":"GEN.1","number":"1","bookId":"GEN","content":"<p class=\\"p\\">In the beginning God created heaven, and earth.</p>","next":{"id":"GEN.2"},"previous":{"id":"GEN.intro"}}}'

chapter_return = {
    "id": "GEN.1",
    "number": "1",
    "bookId": "GEN",
    "content": '<p class="p">In the beginning God created heaven, and earth.</p>',
    "next": {"id": "GEN.2"},
    "previous": {"id": "GEN.intro"},
}


@patch("api_bible_sdk.api_bible.requests.get")
def test_chapters(mock_get):
    mock_get().text = chapters_response
    bible_py = APIBible("1234")
    chapters = bible_py.get_chapters("56", "78")
    assert len(chapters) == 2
    assert chapters == chapters_return


@patch("api_bible_sdk.api_bible.requests.get")
def test_chapter(mock_get):
    mock_get().text = chapter_response
    bible_py = APIBible("1234")
    chapter = bible_py.get_chapter("56", "78")
    assert len(chapter) == 6
    assert chapter == chapter_return
