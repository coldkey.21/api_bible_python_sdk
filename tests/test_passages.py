import pytest
from api_bible_sdk import APIBible
from unittest.mock import Mock, patch

passages_response = '{"data": {"id": "MAT.1.1-MAT.1.3","chapterIds": ["MAT.1"],"content": "The book of the generation of Jesus Christ..."} }'
passages_return = {
    "id": "MAT.1.1-MAT.1.3",
    "chapterIds": ["MAT.1"],
    "content": "The book of the generation of Jesus Christ...",
}


@patch("api_bible_sdk.api_bible.requests.get")
def test_passages(mock_get):
    mock_get().text = passages_response
    bible_py = APIBible("1234")
    passages = bible_py.get_passages("56", "MAT.1.1-MAT.1.3")
    assert len(passages) == 3
    assert passages == passages_return
