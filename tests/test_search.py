import pytest
from api_bible_sdk import APIBible
from unittest.mock import Mock, patch

search_response = '{ "data": { "query": "noah", "limit": 10, "offset": 0, "total": 59, "verses": [ { "id": "GEN.6.9" }, { "id": "GEN.5.32" } ] } }'
search_return = {
    "query": "noah",
    "limit": 10,
    "offset": 0,
    "total": 59,
    "verses": [{"id": "GEN.6.9"}, {"id": "GEN.5.32"}],
}


@patch("api_bible_sdk.api_bible.requests.get")
def test_search(mock_get):
    mock_get().text = search_response
    api_bible = APIBible("1234")
    search = api_bible.search("de4e12af7f28f599-01", "noah")
    assert search == search_return
