import pytest
from api_bible_sdk import APIBible
from unittest.mock import Mock, patch


book_sections_response = '{"data": [{"id": "MRK.S1", "firstVerseId": "MRK.1.1", "lastVerseId": "MRK.1.8"}, {"id": "MRK.S2", "firstVerseId": "MRK.1.9", "lastVerseId": "MRK.1.11"}]}'
book_sections_return = [
    {"id": "MRK.S1", "firstVerseId": "MRK.1.1", "lastVerseId": "MRK.1.8"},
    {"id": "MRK.S2", "firstVerseId": "MRK.1.9", "lastVerseId": "MRK.1.11"},
]

chapter_sections_response = '{"data": [{"id": "MRK.S1", "firstVerseId": "MRK.1.1", "lastVerseId": "MRK.1.8"}, {"id": "MRK.S2", "firstVerseId": "MRK.1.9", "lastVerseId": "MRK.1.11"}]}'
chapter_sections_return = [
    {"id": "MRK.S1", "firstVerseId": "MRK.1.1", "lastVerseId": "MRK.1.8"},
    {"id": "MRK.S2", "firstVerseId": "MRK.1.9", "lastVerseId": "MRK.1.11"},
]

section_response = '{"data": {"id": "MRK.S1", "bibleId": "0c2ff0a5c8b9069c-01", "firstVerseId": "MRK.1.1", "lastVerseId": "MRK.1.8"} }'
section_return = {
    "id": "MRK.S1",
    "bibleId": "0c2ff0a5c8b9069c-01",
    "firstVerseId": "MRK.1.1",
    "lastVerseId": "MRK.1.8",
}


@patch("api_bible_sdk.api_bible.requests.get")
def test_book_sections(mock_get):
    mock_get().text = book_sections_response
    api_bible = APIBible("1234")
    book_sections = api_bible.get_book_sections("0c2ff0a5c8b9069c-01", "MRK")
    assert len(book_sections) == 2
    assert book_sections == book_sections_return


@patch("api_bible_sdk.api_bible.requests.get")
def test_chapter_sections(mock_get):
    mock_get().text = chapter_sections_response
    api_bible = APIBible("1234")
    chapter_sections = api_bible.get_chapter_sections("0c2ff0a5c8b9069c-01", "MRK.1")
    assert len(chapter_sections) == 2
    assert chapter_sections == chapter_sections_return


@patch("api_bible_sdk.api_bible.requests.get")
def test_sections(mock_get):
    mock_get().text = section_response
    api_bible = APIBible("1234")
    section = api_bible.get_section("0c2ff0a5c8b9069c-01", "MRK.S1")
    assert section == section_return
