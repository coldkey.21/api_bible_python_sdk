import pytest
from api_bible_sdk import APIBible
from unittest.mock import Mock, patch

books_response = '{"data":[{"id":"GEN"},{"id":"EXO"},{"id":"MAT"}]}'
books_return = [{"id": "GEN"}, {"id": "EXO"}, {"id": "MAT"}]

book_response = '{"data":{"id":"MAT", "name":"Matthew"}}'
book_return = {"id": "MAT", "name": "Matthew"}


@patch("api_bible_sdk.api_bible.requests.get")
def test_books(mock_get):
    mock_get().text = books_response
    bible_py = APIBible("1234")
    books = bible_py.get_books("5678")
    assert len(books) == 3
    assert books == books_return


@patch("api_bible_sdk.api_bible.requests.get")
def test_book(mock_get):
    mock_get().text = book_response
    bible_py = APIBible("1234")
    books = bible_py.get_book("5678", "MAT")
    assert books == book_return
