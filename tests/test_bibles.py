import pytest
from api_bible_sdk import APIBible
from unittest.mock import Mock, patch

bibles_res = '{"data":[{"id": "0c2ff0a5c8b9069c-01"}, {"id": "b7ad344da9c39262-01"}, {"id": "34690fafc7ffd7d4-02"}]}'
bibles_ret = [
    {"id": "0c2ff0a5c8b9069c-01"},
    {"id": "b7ad344da9c39262-01"},
    {"id": "34690fafc7ffd7d4-02"},
]
bible_res = '{"data":{"id": "0c2ff0a5c8b9069c-01"}}'
bible_ret = {"id": "0c2ff0a5c8b9069c-01"}


@patch("api_bible_sdk.api_bible.requests.get")
def test_bibles(mock_get):
    mock_get().text = bibles_res
    bible_py = APIBible("1234")
    bibles = bible_py.get_bibles()
    assert len(bibles) == 3
    assert bibles == bibles_ret


@patch("api_bible_sdk.api_bible.requests.get")
def test_bibles_with_params(mock_get):
    mock_get().text = bibles_res
    bible_py = APIBible("1234")
    bibles = bible_py.get_bibles(
        language="eng", abbreviation="WEB", name="World English Bible", ids="1,2,3"
    )
    assert len(bibles) == 3
    assert bibles == bibles_ret


@patch("api_bible_sdk.api_bible.requests.get")
def test_bible(mock_get):
    mock_get().content = bible_res
    bible_py = APIBible("1234")
    bible = bible_py.get_bible("0c2ff0a5c8b9069c-01")
    assert bible == bible_ret
