# API.Bible SDK

A Python SDK for [API.Bible](https://scripture.api.bible/).


## Getting Started

### Prerequisites

#### [Python 3.7](https://www.python.org/) or greater

Follow the instruction on the [Python Download Page](https://www.python.org/downloads/)

### Quickstart

#### Install the package

```
$ pip install api_bible_sdk
```

#### Create an application and get the key

[API Bible Applications](https://scripture.api.bible/admin/applications)

#### Copy the following code to list the available bibles, don't forget to use your API key

```
from api_bible_sdk import APIBible

api_bible = APIBible("Your API Key")
print(api_bible.get_bibles())
```

## Built With

* [api.bible](https://scripture.api.bible/) - The backend API
* [Python](https://www.python.org/) - The programming language
* [Pipenv](https://docs.pipenv.org/en/latest/) - Dependency managment

## Authors

* **Matthew Birky** - *Initial work* - [mbirky](https://gitlab.com/coldkey.21)


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
