# Contributing to the Python API Bible SDK
Thank you for taking an interest in the python API Bible SDK and considering taking the time to contribute. Any help or suggestions you may have will be much appreciated. If you have the time open a merge request and I will do my best to get to it promptly.


## Dev Setup

### Install [Pipenv](https://docs.pipenv.org/en/latest/)

```
$ pip install pipenv
```

### Installing Dependencies

Use pipenv to install the dependencies.

```
$ pipenv install
```

## Running the tests

Pytest is installed as part of the dependencies, run the tests with

```
$ pipenv run pytest
```

### Linting

Black is installed as part of the dependencies, run the linting with

```
pipenv run black api_bible_sdk/ tests/
```